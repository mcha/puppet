#!/bin/bash
#
# Script used to launch default container images to finish setup
# @author : Michel CHA

NETWORK=mca
DOMAIN_NAME=mca.fr
PMASTER_NAME=pmaster
HOST1_NAME=host1
HOST2_NAME=host2
FOREMAN_NAME=fmaster

PMASTER_IP=172.18.0.2
HOST1_IP=172.18.0.3
HOST2_IP=172.18.0.4
FOREMAN_IP=172.18.0.5

DNS1=8.8.8.8
DNS2=8.8.4.4

source ./demo/functions.sh

case "$1" in
        pmaster)
            check_if_running_and_delete_if_exist p3_$PMASTER_NAME
            run_container   $PMASTER_NAME \
                            $PMASTER_IP \
                            p3_$PMASTER_NAME \
                            mcha/puppet3_master
          ;;
        host1)
            check_if_running_and_delete_if_exist p3_$HOST1_NAME
            run_container   $HOST1_NAME \
                            $HOST1_IP \
                            p3_$HOST1_NAME \
                            mcha/puppet3_client
            ;;
        host2)
            check_if_running_and_delete_if_exist p3_$HOST2_NAME
            run_container   $HOST2_NAME \
                            $HOST2_IP \
                            p3_$HOST2_NAME \
                            mcha/puppet3_client
            ;;
        fmaster)
            check_if_running_and_delete_if_exist p3_$FOREMAN_NAME
            run_container   $FOREMAN_NAME \
                            $FOREMAN_IP \
                            p3_$FOREMAN_NAME \
                            mcha/puppet3_foreman \
                            "-p 8090:80 -p 5432:5432 -p 8443:443"
            ;;
        *)
            echo $"Usage: $0 {pmaster|host1|host2|fmaster}"
            exit 1
esac
