#!/bin/bash

PUPPET_CA_DIR=/var/lib/puppet/ssl
CA_FILE=$PUPPET_CA_DIR/crl.pem

service apache2 stop
rm -rf $PUPPET_CA_DIR

# Start puppet => will generate new CA and RSA keys
puppet master

echo -n "Waiting for $CA_FILE to be created "
while [ ! -f "$CA_FILE" ]
do
    inotifywait -qqt 3 -e create -e moved_to "$(dirname $CA_FILE)"
    echo -n "."
done
echo
# In case you want to run it, in non daemon mode
#puppet master --verbose --no-daemonize

#sed -ri "s/ssl\/(.*)\.lan\.pem/ssl\/certs\/puppet\.pem/g" /etc/apache2/sites-enabled/puppetmaster.conf

# Restart Puppet passenger
service apache2 stop && service apache2 start
