#!/bin/bash

#
# Use this to start/stop all needed services.
#
# @author : Michel CHA


case "$1" in
        start)
            service ntp $1
            service postgresql $1
            sleep 10
            service apache2 $1
            service puppet $1
            service foreman $1
            service foreman-proxy $1
            ;;
        stop)
            service foreman $1
            service foreman-proxy $1
            service apache2 $1
            service puppet $1
            service postgresql $1
            service ntp $1
            ;;
        restart)
            $0 stop && $0 start
            ;;
        *)
            echo $"Usage: $0 {start|stop|restart}"
            exit 1
esac
