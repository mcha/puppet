#!/bin/bash

ACTUAL_VERSION=$(puppet --version)
sed -ri "s/PUPPET_VERSION/$ACTUAL_VERSION/g" /etc/apt/preferences.d/00-puppet.pref
