#!/bin/bash

#
# Commands to launch within fresh running container once image is built
#
# @author : Michel CHA

# ensures that facter fqdn == hostname -f
sed -r s/$(hostname)/$(hostname)\.mca\.fr/g /etc/hosts | uniq > hosts && cat hosts > /etc/hosts
sed -r s/lan/mca\.fr/g /etc/resolv.conf > resolv.conf && cat resolv.conf > /etc/resolv.conf

# launch the foreman installer
foreman-installer
# enable diff outputs in puppet
sed -ri s/show_diff.*/show_diff=true/g /etc/puppet/puppet.conf

# enables puppet agent if disabled by default
puppet agent enable && puppet agent -t

# install ntp modules
puppet module install -i /etc/puppet/environments/production/modules saz/ntp

# activate foreman service boot-up
sed -r s/"START=no"/"START=yes"/g /etc/default/foreman > foreman && cat foreman > /etc/default/foreman
rm -f foreman

# enable http for convenience => not an issue for dev/demo purpose
sed -r s/":require_ssl: true"/":require_ssl: false"/g /etc/foreman/settings.yaml > settings.yaml && cat settings.yaml > /etc/foreman/settings.yaml
rm -f settings.yaml

#
# Command that might be useful in case foreman-installer does not create the database properly
# foreman-rake -T       : display available tasks
# foreman-rake db:setup : will create the db and seed default values
# or  foreman-rake db:create
#     foreman-rake db:seed
# foreman-rake permissions:reset : will reset admin pwd
# in case you forgot it
#
