#!/bin/bash
#
# Script used to launch foreman dedicated containers
# @author : Michel CHA

NETWORK=foreman
DOMAIN_NAME=mca.fr
FOREMAN_NAME=fmaster
HOST1_NAME=host1
HOST2_NAME=host2

FOREMAN_IP=192.168.10.2
HOST1_IP=192.168.10.3
HOST2_IP=192.168.10.4

DNS1=8.8.8.8
DNS2=8.8.4.4

source functions.sh

case "$1" in
        host1)
            check_if_running_and_delete_if_exist p3_foreman_$HOST1_NAME
            run_container   $HOST1_NAME \
                            $HOST1_IP \
                            p3_foreman_$HOST1_NAME \
                            mcha/puppet3_demo_foreman_$HOST1_NAME
            ;;
        host2)
            check_if_running_and_delete_if_exist p3_foreman_$HOST2_NAME
            run_container   $HOST2_NAME \
                            $HOST2_IP \
                            p3_foreman_$HOST2_NAME \
                            mcha/puppet3_demo_foreman_$HOST2_NAME
            ;;
        fmaster)
            check_if_running_and_delete_if_exist p3_$FOREMAN_NAME
            run_container   $FOREMAN_NAME \
                            $FOREMAN_IP \
                            p3_$FOREMAN_NAME \
                            mcha/puppet3_demo_foreman_$FOREMAN_NAME \
                            "-p 8090:80 -p 5432:5432 -p 8443:443"
            ;;
        *)
            echo $"Usage: $0 {host1|host2|fmaster}"
            exit 1
esac
