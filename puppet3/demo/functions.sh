#
# Common functions
# @author : Michel CHA

function check_if_running_and_delete_if_exist {
    if [ -n "$(docker ps -q -f name=$1)" ]; then
        echo "> $1 is already started !"
        exit 1;
    fi
    if [ -n "$(docker ps -aq -f status=exited -f name=$1)" ]; then
        echo "> deleting previous stopped container $1"
        docker rm $1 1>/dev/null
    fi
}

function run_container {
    local HOST_NAME=$1
    local HOST_IP=$2
    local CONTAINER_NAME=$3
    local IMAGE_TAG=$4
    local PORTS=$5
    docker run -it --net $NETWORK --ip $HOST_IP --hostname $HOST_NAME --network-alias "$HOST_NAME.$DOMAIN_NAME" \
         --dns-search=$DOMAIN_NAME --dns=$DNS1 --dns=$DNS2 \
         --name $CONTAINER_NAME \
         $PORTS \
         $IMAGE_TAG bash
}
