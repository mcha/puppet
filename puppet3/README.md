##Description

Source code to build Puppet 3.x images and run demo containers.

Source repository (including launcher scripts) : https://bitbucket.org/mcha/puppet/

## Content

* client : 
> allows to create a image with puppet 3.x agent inside 
* demo : 
> starter scripts to launch demo containers
* master : 
> allows to create a image with puppet 3.x master + agent inside
* foreman : 
> allows to create a image with foreman + puppet 3.x inside



		├── client
		│   ├── resources
		│   │   ├── etc
		│   │   ├── services.sh				(use this to start/stop puppet agent service)
		│   │   ├── setup_cert.sh			(use this to setup new puppet agent cert)
		│   │   ├── start.sh
		│   │   └── stick_puppet_version.sh
		│   └── Dockerfile
		├── demo
		│   ├── foreman_start.sh				(use this to start foreman demo containers)
		│   └── puppet_start.sh					(use this to start puppet demo containers)
		├── foreman
		│   ├── foreman_client
		│   │   ├── resources
		│   │   └── Dockerfile
		│   ├── resources
		│   │   ├── etc
		│   │   ├── create_dbuser.sql
		│   │   ├── services.sh				(use this to start/stop foreman services)
		│   │   ├── setup_cert.sh			(use this to setup new puppet master cert)
		│   │   ├── setup_foreman.sh		(use this to install foreman)
		│   │   ├── start.sh				(place any default command here)
		│   │   └── stick_puppet_version.sh
		│   └── Dockerfile
		├── master
		│   ├── resources
		│   │   ├── etc
		│   │   ├── services.sh
		│   │   ├── setup_cert.sh				(use this to setup new puppet master cert)
		│   │   ├── start.sh
		│   │   └── stick_puppet_version.sh 	(use this to start/stop puppet master services)
		│   └── Dockerfile
		├── README.md
		├── staging_boot.sh 			(use this to start fresh container and configure them)



##Usage

### First step
Build the needed docker networks with the provided scripts : 

* foreman_network_setup.sh
* staging_network_setup.sh
* puppet_network_setup.sh

### How to create your images

	1. Use staging_boot.sh to start containers from freshly built images (will connect to them to **mca** network)
	2. Finish their configuration by generating certs + signing etc.
	3. Commit your changes to given TAG name using docker commit 
	4. loop until you have all your needed images
	
### Launch demo containers

2 Sets of pre-configured Demo images were created using the process described above :

* Puppet master only => to be started with `puppet_start.sh` script
	* **puppet3_demo_pmaster** : `./puppet_start.sh pmaster`
	* **puppet3_demo_host1** : `./puppet_start.sh host1`
	* **puppet3_demo_host2** : `./puppet_start.sh host2`
	
*  Foreman => to be started with `foreman_start.sh` script
	* **puppet3_demo_foreman_fmaster** : `./foreman_start.sh fmaster`
	* **puppet3_demo_foreman_host1**  : `./foreman_start.sh host1`
	* **puppet3_demo_foreman_host2** : `./foreman_start.sh host2`

These images are provided for educational purpose to ease learning for anyone interested in puppet 3.x and foreman.

Look at the **README.md** inside the demo folder.

### FAQ
#### Where to find the built docker images

* On the docker hub : https://hub.docker.com/r/mcha/

#### Foreman 
* Default admin password : cat /root/admin_password.txt
* Url : 
	* http : http://localhost:8090/ (was enabled to ease access)
	* https : https://localhost:8443/ (you will have security warning in your browser)
* PostgreSQL port : 5342 (redirection is done)

#### Why the demo docker network dns is .mca.fr ?
* Ymca => Why mca ? It's a joke : mca is the trigram for mcha

##Interesting links
* [Puppet (mini tuto)](https://www.digitalocean.com/community/tutorials/how-to-install-puppet-to-manage-your-server-infrastructure) 
* [Writing Puppet 3.x Manifest](https://www.digitalocean.com/community/tutorials/configuration-management-101-writing-puppet-manifests) 
* [Puppet forge](https://forge.puppet.com/puppetlabs) 
* [Puppet 3.x reference](https://docs.puppet.com/puppet/3/) 
* [Foreman (mini tuto)](https://www.digitalocean.com/community/tutorials/how-to-use-foreman-to-manage-puppet-nodes-on-ubuntu-14-04) 
* [Foreman Setup](https://www.theforeman.org/manuals/1.14/index.html) 
* [Hiera](https://docs.puppet.com/hiera/3.3/complete_example.html)
* [Migrating from Puppet 3.x to Puppet 4.x](http://projects.theforeman.org/projects/foreman/wiki/Upgrading_from_Puppet_3_to_4)

### License
* This work is published under the Creative Common BY-SA License for educational purpose : you can use this content, copy, modify it freely, just mention the original author mcha.

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
mcha/puppet by [Michel Cha](https://bitbucket.org/mcha/puppet) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).