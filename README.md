## Puppet & Foreman Docker demo

### Description

Provide Dockerfiles to build puppet and foreman docker images.

Source repository (including launcher scripts) : https://bitbucket.org/mcha/puppet/

Follow the guides in the Readme.md

### Versions

|        			|     Versions     	| 
| ------------- 		| -------------   		| 
| Puppet 3        	|     3.7.6			|
| Puppet 4		|     4.9.2			|
| Foreman		|     1.14     		|

### Content

	├── puppet3		(see inner README.md)
	├── puppet4		(see inner README.md)
	├── README.md
	├── foreman_network_setup.sh 	(use this to setup the docker network for foreman demo containters)
	├── staging_network_setup.sh		(use this to setup the docker network for setuping containters)
	└── puppet_network_setup.sh		(use this to setup the docker network for puppet only demo containters)

## Usage
### First step
Build the needed docker networks for first boot and demo usage with the provided scripts : 

* foreman_network_setup.sh
* staging_network_setup.sh
* puppet_network_setup.sh

### Next
Either goto puppet3 or puppet4 and read the REAME.md within to continue.


### FAQ
#### Where to find the built docker images

* On the docker hub : https://hub.docker.com/r/mcha/

#### Foreman configuration
* Default admin password : cat /root/admin_password.txt
* Url : 
	* http : http://localhost:8090/ (was enabled to ease access)
	* https : https://localhost:8443/ (you will have security warning in your browser)
* PostgreSQL port : 5342 (redirection is done)

#### Why the demo docker network dns is .mca.fr ?
* Ymca => Why mca ? It's a joke : mca is the trigram for mcha
 

### License
* This work is published under the Creative Common BY-SA License for educational purpose : you can use this content, copy, modify it freely, just mention the original author mcha.

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
mcha/puppet by [Michel Cha](https://bitbucket.org/mcha/puppet) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).