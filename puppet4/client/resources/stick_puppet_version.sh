#!/bin/bash

export PATH=/opt/puppetlabs/bin:$PATH

ACTUAL_VERSION=$(dpkg -s puppet-agent | grep Version | cut -d: -f2)
sed -ri "s/PUPPET_VERSION/$ACTUAL_VERSION/g" /etc/apt/preferences.d/00-puppet.pref
