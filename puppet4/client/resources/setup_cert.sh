#!/bin/bash

#
# puppet cert list --all
# puppet cert <sign|revoke|clean> entry
#
# @author : Michel CHA

# Ensure new cert are generated + csr
rm -rf /opt/puppetlabs/puppet/ssl /etc/puppetlabs/puppet/ssl/
# new certs will be generated in /etc/puppetlabs/puppet/ssl/
puppet agent -t
