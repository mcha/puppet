--
-- Connect to Postgresql
-- su - postgres
-- psql
--
-- psql -d foreman -f create_dbuser.sql
-- psql -U foreman -d foreman -W

CREATE USER foreman WITH PASSWORD 'fijT3N7fePNf6fqw6o8NUwNg4GqEzMbW';
CREATE DATABASE foreman;
GRANT ALL PRIVILEGES ON DATABASE foreman to foreman;
ALTER USER foreman CREATEDB;
ALTER USER foreman WITH SUPERUSER;

-- Cheat sheet :
--
-- \c     : change db
-- \list  : list database
-- \du    : list pgsql users
-- \db    : list tablespaces
-- \d     : list tables
-- \cd yy : cd in given directory 
-- \i x.sql : execute given script
-- \?     : display available commands
