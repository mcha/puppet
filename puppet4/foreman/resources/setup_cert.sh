#!/bin/bash

#
# puppet cert list --all
# puppet cert <sign|revoke|clean> entry
#
# @author : Michel CHA

service puppetserver stop

rm -rf /opt/puppetlabs/puppet/ssl /etc/puppetlabs/puppet/ssl/

# Start puppet => will generate new CA and RSA keys
service puppetserver start


