#!/bin/bash
#
# Script used to launch raw puppet (no foreman) dedicated containers
# @author : Michel CHA

NETWORK=puppet

DOMAIN_NAME=mca.fr
PMASTER_NAME=pmaster
HOST1_NAME=host1
HOST2_NAME=host2

PMASTER_IP=192.168.20.2
HOST1_IP=192.168.20.3
HOST2_IP=192.168.20.4

DNS1=8.8.8.8
DNS2=8.8.4.4

source functions.sh

case "$1" in
        pmaster)
            check_if_running_and_delete_if_exist p4_$PMASTER_NAME
            run_container   $PMASTER_NAME \
                            $PMASTER_IP \
                            p4_$PMASTER_NAME \
                            mcha/puppet4_demo_$PMASTER_NAME
          ;;
        host1)
            check_if_running_and_delete_if_exist p4_$HOST1_NAME
            run_container   $HOST1_NAME \
                            $HOST1_IP \
                            p4_$HOST1_NAME \
                            mcha/puppet4_demo_$HOST1_NAME
            ;;
        host2)
            check_if_running_and_delete_if_exist p4_$HOST2_NAME
            run_container   $HOST2_NAME \
                            $HOST2_IP \
                            p4_$HOST2_NAME \
                            mcha/puppet4_demo_$HOST2_NAME
            ;;
        *)
            echo $"Usage: $0 {pmaster|host1|host2}"
            exit 1
esac
