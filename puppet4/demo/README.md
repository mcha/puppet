### Description

This demo directory contains scripts to launch the demo containers.

In the rest of this document the  `x` in `puppetx` is the version of puppet you wish to use.


### Usage

#### Pre-requisite

`docker pull` the demo images set you want to use.

#### Launch demo containers

2 Sets of pre-configured Demo images were created.

Certificate and keys within each set of Demo images are pre-configured so that the images are ready to use.
In order to launch them use the start script that you will find in the `demo` folder on the Source repository. 

* Puppet master only => to be started with `puppet_start.sh` script
	* **puppetx_demo_pmaster** : `./puppet_start.sh pmaster`
	* **puppetx_demo_host1** : `./puppet_start.sh host1`
	* **puppetx_demo_host2** : `./puppet_start.sh host2`
	
*  Foreman => to be started with `foreman_start.sh` script
	* **puppetx_demo_foreman_fmaster** : `./foreman_start.sh fmaster`
	* **puppetx_demo_foreman_host1**  : `./foreman_start.sh host1`
	* **puppetx_demo_foreman_host2** : `./foreman_start.sh host2`

These images are provided for educational purpose to ease learning for anyone interested in puppet 4.x and foreman.

#### Scripts available in demo containers

* mcha/puppetx_demo_foreman_host1 (same as host2)
* mcha/puppetx_demo_foreman_host2

		│   ├── root
		│   │   ├── services.sh				(use this to start/stop puppet agent service)
		│   │   ├── setup_cert.sh			(use this to setup new puppet agent cert)
		│   │   ├── start.sh
		│   └── Dockerfile

* mcha/puppetx_demo_foreman_fmaster

		│   ├── root
		│   │   ├── services.sh				(use this to start/stop foreman services)
		│   │   ├── setup_cert.sh			(use this to setup new puppet master cert)
		│   │   ├── setup_foreman.sh		(use this to install foreman -> useless in demo container)
		│   │   ├── start.sh				(place any default command 

* mcha/puppetx_demo_foreman_pmaster

		│   ├── root
		│   │   ├── etc
		│   │   ├── services.sh
		│   │   ├── setup_cert.sh				(use this to setup new puppet master cert)
		│   │   ├── start.sh


### FAQ

#### How to create your own images

	1. Use lboot.sh to start freshly built containers
	2. Finish their configuration by generating certs + signing etc.
	3. Commit your changes to given TAG name using docker commit 
	4. loop until you have all your needed images

#### Where to find the built docker images

* On the docker hub : https://hub.docker.com/r/mcha/

#### Foreman 
* Default admin password : cat /root/admin_password.txt
* Url : 
	* http : http://localhost:8090/ (was enabled to ease access)
	* https : https://localhost:8443/ (you will have security warning in your browser)
* PostgreSQL port : 5342 (redirection is done)

#### Why the demo docker network dns is .mca.fr ?
* Ymca => Why mca ? It's a joke : mca is the trigram for mcha

### Interesting links
* [Foreman (mini tuto)](https://www.digitalocean.com/community/tutorials/how-to-use-foreman-to-manage-puppet-nodes-on-ubuntu-14-04) 
* [Foreman Setup](https://www.theforeman.org/manuals/1.14/index.html) 
* [Puppet 4.x syntax & style guide](https://docs.puppet.com/puppet/4.9/style_guide.html) 
* [Hiera](https://docs.puppet.com/hiera/3.3/complete_example.html)
* [Migrating from Puppet 3.x to Puppet 4.x](http://projects.theforeman.org/projects/foreman/wiki/Upgrading_from_Puppet_3_to_4)

### License
* This work is published under the Creative Common BY-SA License for educational purpose : you can use this content, copy, modify it freely, just mention the original author mcha.

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
mcha/puppet by [Michel Cha](https://bitbucket.org/mcha/puppet) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).