#!/bin/bash

#
# Use this to start/stop all needed services.
#
# @author : Michel CHA


case "$1" in
        start)
            service ntp $1
            service puppetserver $1
            ;;
        stop)
            service puppetserver $1
            service ntp $1
            ;;
        restart)
            $0 stop && $0 start
            ;;
        *)
            echo $"Usage: $0 {start|stop|restart}"
            exit 1
esac
